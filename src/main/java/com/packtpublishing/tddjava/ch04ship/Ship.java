package com.packtpublishing.tddjava.ch04ship;

public class Ship {

//	public Ship(Location location){
//		super();
//		this.location = location;
//	}
	
	public Ship(Location location, Planet planet) {
		super();
		this.location = location;
		this.planet = planet;
	}

	private Location location;
	private Planet planet;
	
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public boolean moveForward() {
		//return location.forward();
		return location.forward(planet.getMax(),planet.getObstacles());
	}

	public boolean moveBackward() {
		//return location.backward();
		return location.backward(planet.getMax(),planet.getObstacles());
	}

	public void turnLeft() {
		location.turnLeft();
	}

	public void turnRight() {
		location.turnRight();		
	}

	public String receiveCommands(String commands) {
		StringBuilder output = new StringBuilder();
		for (int i= 0; i<commands.length(); i++) {
			boolean status = true;
			if(commands.charAt(i) == 'r'){
				turnRight();
			}
			if(commands.charAt(i) == 'f'){
				status = moveForward();
			}
			if(commands.charAt(i) == 'l'){
				turnLeft();
			}
			if(commands.charAt(i) == 'b'){
				status = moveBackward();
			}
			if(status){
				output.append("O");
			}else{
				output.append("X");
			}
		}
				
		return output.toString();
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	

}
