package com.packtpublishing.tddjava.ch04ship;

import org.testng.annotations.*;

import static org.testng.Assert.*;

import java.util.ArrayList;
import java.util.List;

@Test
public class ShipSpec {
	
	private Location location;
	private Ship ship;
	private Planet planet;
	
	
	@BeforeMethod
	public void beforeTest(){
		Point max = new Point(50, 50);
		location = new Location(new Point(21, 13),
				Direction.NORTH);
		List<Point> obstacles = new ArrayList<>();
		obstacles.add(new Point(44, 44));
		obstacles.add(new Point(45, 46));
		planet = new Planet(max,obstacles);
		ship = new Ship(location, planet);
		//ship = new Ship(location);
	}
	
	public void whenGetLocationThenGetDirection(){
		
		assertEquals(ship.getLocation(), location);
	}
		
	
	public void whenMoveForwardThenForward(){
		Location expected = location.copy();
		expected.forward();
		ship.moveForward();
		assertEquals(ship.getLocation(), expected);
	}
	
	public void whenMoveBackwardThenBackWard(){
		Location expected = location.copy();
		expected.backward();
		ship.moveBackward();
		assertEquals(ship.getLocation(), expected);
	}
	
	public void whenMoveTurnLeftThenMoveLeft(){
		Location expected = location.copy();
		expected.turnLeft();
		ship.turnLeft();
		assertEquals(ship.getLocation(), expected);
	}
	
	public void whenMoveTurnRightThenMoveRight(){
		Location expected = location.copy();
		expected.turnRight();
		ship.turnRight();
		assertEquals(ship.getLocation(), expected);
		
	}
	public void whenMoveTurnLeftThenMoveRight(){
		Location expected = location.copy();
		expected.turnRight();
		ship.turnRight();
		assertEquals(ship.getLocation(), expected);
		
	}
	
	public void whenReceivedCommandsFThenForward(){
		Location expected = location.copy();
		expected.forward();
		ship.receiveCommands("f");
		assertEquals(ship.getLocation(), expected);
	}
	
	public void whenReceiveCommandsBThenBackward(){
		Location expected = location.copy();
		expected.backward();
		ship.receiveCommands("b");
		assertEquals(ship.getLocation(), expected);
	}
	
	public void whenReceiveCommandsLThenMoveLeft(){
		Location expected = location.copy();
		expected.turnLeft();
		ship.receiveCommands("l");
		assertEquals(ship.getLocation(), expected);
	}
	
	public void whenReceiveCommandsRThenMoveRight(){
		Location expected = location.copy();
		expected.turnRight();
		ship.receiveCommands("r");
		assertEquals(ship.getLocation(), expected);
	}

	public void whenReceiveCommandsThenAllAreExecuted(){
		Location expected = location.copy();
		expected.turnRight();
		expected.forward();
		expected.turnLeft();
		expected.backward();
		ship.receiveCommands("rflb");
		assertEquals(ship.getLocation(), expected);
	}
	
	public void whenInstantiatedThenPlanetIsStored(){
    	//Point max = new Point(50, 50);
    	//Planet planet = new Planet(max);
    	//ship = new Ship(location, planet);
    	assertEquals(ship.getPlanet(), planet);
    }
	
	public void givenDirectionEAndXEqualsMaxXWhenReceiveCommandsFThenWrap(){
		location.setDirection(Direction.EAST);
		location.getPoint().setX(planet.getMax().getX());
		ship.receiveCommands("f");
		assertEquals(location.getX(), 1);
	}
	
	public void givenDirectionEAndXEquals1WhenReceiveCommandsBThenWrap(){
		location.setDirection(Direction.EAST);
		location.getPoint().setX(1);
		ship.receiveCommands("b");
		assertEquals(location.getX(), planet.getMax().getX());
	}
	
	public void whenReceiveCommandsThenStopOnObstacle(){
		List<Point> obstacles = new ArrayList<>();
		obstacles.add(new Point(location.getX() + 1, location.getY()));
		ship.getPlanet().setObstacles(obstacles);
		Location expected = location.copy();
		expected.turnRight();
		// Moving forward would encouter an obstacle
		//expected.forward(new Point(0,0),new ArrayList<Point>())
		expected.turnLeft();
		expected.backward(new Point(0, 0), new ArrayList<Point>());
		ship.receiveCommands("rflb");
		assertEquals(ship.getLocation(), expected);
		
	}
	
	public void whenReceiveCommandsThenOForOkAndXForObstacle(){
		List<Point> obstacles = new ArrayList<>();
		obstacles.add(new Point(location.getX()+1, location.getY()));
		ship.getPlanet().setObstacles(obstacles);
		String status = ship.receiveCommands("rflb");
		assertEquals(status, "OXOO");
	}
	
}
